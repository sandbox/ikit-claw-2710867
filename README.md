## About Breeze
-----------
 - Breeze is based on the Joomla Breeze theme by OSTraining reproduced with the consent of OSTraining for use on Drupal.
 - Some files are required for this theme that are covered under the MIT license and as such are not permitted to be added to drupal.org see documention on drupal and licensing below
 - The Drupal Licensing FAQ https://www.drupal.org/about/licensing
 - 3rd party libraries and content on Drupal.org https://www.drupal.org/node/422996
 - Why drupal.org doesn't host GPL-"compatible" code https://www.drupal.org/node/66113

## Prerequisites
-----------
 - This theme requires Bootstrap which can be located on github.
 - BootStrap https://github.com/twbs/bootstrap
 - Before you install the theme you should download bootstrap and go through the setup stage below.

## Setup
-----------
 - Download and extract BootStrap https://github.com/twbs/bootstrap ,  
 - copy bootstrap.js from bootstrap-master/bootstrap-master/dist/js into Breeze/js/bootstrap/  
 - copy bootstrap.css from bootstrap-master/bootstrap-master/dist/css into Breeze/css/bootstrap/
 - copy bootstrap-theme.css from bootstrap-master/bootstrap-master/dist/css into Breeze/css/bootstrap/

 - The content of the /bootstrap folders should never be edited this is 3rd party code.

## Maintainer
----------
 - Feel free to contact me
 - dclpuk@gmail.com

## FUTURE IMPROVEMENTS
--------------------
 - 
